cmake_minimum_required(VERSION 3.5)

project(NVMLpp LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if("${CMAKE_CXX_COMPILER}" MATCHES "mingw")
  set(BUILD_WITH_MINGW ON)
endif()

file(GLOB_RECURSE HEADERS include/*.hpp)
file(GLOB_RECURSE SOURCES src/*.cpp)

include_directories(include/)
include_directories(lib/)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

add_library(${PROJECT_NAME} STATIC ${SOURCES} ${HEADERS})

if(BUILD_WITH_MINGW)
  target_link_libraries(${PROJECT_NAME} nvml)
else()
  target_link_libraries(${PROJECT_NAME} nvidia-ml)
endif()
