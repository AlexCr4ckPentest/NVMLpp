#include "nvmlpp/nvmlpp_session.hpp"
#include "nvmlpp/util/throw_if_error.hpp"

NVMLpp::Session::Session()
    : nvml_devices_ {}
    , nvml_units_ {}
{
    NVMLPP_MAKE_NATIVE_CALL(NVML_native::nvmlInit());
}

NVMLpp::Session::~Session()
{
    NVML_native::nvmlShutdown();
}

const std::string& NVMLpp::Session::get_NVML_version() const
{
    static std::string nvml_version_str {};
    if (nvml_version_str.empty())
    {
        char buffer[NVML_SYSTEM_NVML_VERSION_BUFFER_SIZE] {0};
        NVMLPP_MAKE_NATIVE_CALL(
                    NVML_native::nvmlSystemGetNVMLVersion(buffer,
                                                          NVML_SYSTEM_NVML_VERSION_BUFFER_SIZE)
                    );
        nvml_version_str = buffer;
    }
    return nvml_version_str;
}

const std::string& NVMLpp::Session::get_system_driver_version() const
{
    static std::string driver_version_str {};
    if (driver_version_str.empty())
    {
        char buffer[NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE] {0};
        NVMLPP_MAKE_NATIVE_CALL(
                    NVML_native::nvmlSystemGetDriverVersion(buffer,
                                                            NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE)
                    );
        driver_version_str = buffer;
    }
    return driver_version_str;
}

int NVMLpp::Session::get_cuda_version() const
{
    int cuda_driver_version {0};
    NVMLPP_MAKE_NATIVE_CALL(NVML_native::nvmlSystemGetCudaDriverVersion_v2(&cuda_driver_version));
    return cuda_driver_version;
}

const std::vector<NVMLpp::NVML_device>& NVMLpp::Session::get_available_devices()
{
    unsigned device_count {0};
    NVMLPP_MAKE_NATIVE_CALL(NVML_native::nvmlDeviceGetCount(&device_count));
    check_and_update<NVML_device>(nvml_devices_, device_count);
    return nvml_devices_;
}

const std::vector<NVMLpp::NVML_unit>& NVMLpp::Session::get_available_units()
{
    unsigned unit_count {0};
    NVMLPP_MAKE_NATIVE_CALL(NVML_native::nvmlUnitGetCount(&unit_count));
    check_and_update<NVML_unit>(nvml_units_, unit_count);
    return nvml_units_;
}

NVMLpp::Session& NVMLpp::Session::instance()
{
    static Session nvml_session {};
    return nvml_session;
}
