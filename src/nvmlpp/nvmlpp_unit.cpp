#include "nvmlpp/nvmlpp_unit.hpp"
#include "nvmlpp/util/throw_if_error.hpp"

#include <utility>

NVMLpp::NVML_unit::NVML_unit(unsigned unit_index)
    : nvml_unit_handle_ {}
{
    NVMLPP_MAKE_NATIVE_CALL(NVML_native::nvmlUnitGetHandleByIndex(unit_index, &nvml_unit_handle_));
}

NVMLpp::NVML_unit::NVML_unit(NVML_native::nvmlUnit_t native_nvml_unit) noexcept
    : nvml_unit_handle_ {}
{
    std::swap(native_nvml_unit, nvml_unit_handle_);
}

NVMLpp::NVML_unit::NVML_unit(NVML_unit&& other) noexcept
{
    std::swap(other.nvml_unit_handle_, nvml_unit_handle_);
}

NVMLpp::NVML_unit& NVMLpp::NVML_unit::operator=(NVML_unit&& other) noexcept
{
    std::swap(other.nvml_unit_handle_, nvml_unit_handle_);
    return *this;
}

NVMLpp::NVML_unit NVMLpp::NVML_unit::from_index(unsigned unit_index) noexcept
{
    return NVML_unit {unit_index};
}

NVMLpp::NVML_unit NVMLpp::NVML_unit::from_native_handle(NVML_native::nvmlUnit_t native_nvml_unit) noexcept
{
    return NVML_unit {native_nvml_unit};
}
