#include <algorithm>

#include "nvmlpp/util/throw_if_error.hpp"
#include "nvmlpp/impl/nvmlpp_device_impl.hpp"

NVMLpp::impl::NVML_Device_impl::NVML_Device_impl(unsigned device_index)
    : nvml_device_handle_   {}
    , index_                {device_index}
    , name_                 {}
    , brand_                {}
    , vendor_               {}
    , subvendor_            {}
    , uuid_                 {}
    , vbios_version_        {}
    , arch_                 {}
    , pci_bus_id_           {}
    , bus_type_             {}
{
    char uuid[NVML_DEVICE_UUID_BUFFER_SIZE] {0};

    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetHandleByIndex(index_, &nvml_device_handle_)
                );
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetUUID(nvml_device_handle_, uuid, NVML_DEVICE_UUID_BUFFER_SIZE)
                );

    uuid_ = uuid;
    initialize_static_info();
}



NVMLpp::impl::NVML_Device_impl::NVML_Device_impl(std::string_view device_uuid)
    : nvml_device_handle_   {}
    , index_                {}
    , name_                 {}
    , brand_                {}
    , vendor_               {}
    , subvendor_            {}
    , uuid_                 {device_uuid}
    , vbios_version_        {}
    , arch_                 {}
    , pci_bus_id_           {}
    , bus_type_             {}
{
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetHandleByUUID(uuid_.c_str(), &nvml_device_handle_)
                );
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetIndex(nvml_device_handle_, &index_)
                );
    initialize_static_info();
}



NVMLpp::impl::NVML_Device_impl::NVML_Device_impl(NVML_Device_impl&& other) noexcept
{
    std::swap(index_, other.index_);
    std::swap(nvml_device_handle_, other.nvml_device_handle_);
    std::swap(name_, other.name_);
    std::swap(brand_, other.brand_);
    std::swap(vendor_, other.vendor_);
    std::swap(subvendor_, other.subvendor_);
    std::swap(uuid_, other.uuid_);
    std::swap(vbios_version_, other.vbios_version_);
    std::swap(arch_, other.arch_);
    std::swap(pci_bus_id_, other.pci_bus_id_);
    std::swap(bus_type_, other.bus_type_);
}



NVMLpp::impl::NVML_Device_impl& NVMLpp::impl::NVML_Device_impl::operator=(NVML_Device_impl&& other) noexcept
{
    std::swap(*this, other);
    return *this;
}



void NVMLpp::impl::NVML_Device_impl::initialize_static_info()
{
    char name[NVML_DEVICE_NAME_BUFFER_SIZE] {0};
    char vbios[NVML_DEVICE_VBIOS_VERSION_BUFFER_SIZE] {0};
    NVML_native::nvmlBrandType_t brand {};
    unsigned arch {0};
    NVML_native::nvmlPciInfo_t pci_info {};
    NVML_native::nvmlBusType_t bus_type {};

    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetName(nvml_device_handle_, name,
                                               NVML_DEVICE_NAME_BUFFER_SIZE)
                );
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetVbiosVersion(nvml_device_handle_, vbios,
                                                       NVML_DEVICE_VBIOS_VERSION_BUFFER_SIZE)
                );
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetBrand(nvml_device_handle_, &brand)
                );
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetArchitecture(nvml_device_handle_, &arch)
                );
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetPciInfo(nvml_device_handle_, &pci_info)
                );
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetBusType(nvml_device_handle_, &bus_type)
                );

    name_ = name;
    vbios_version_ = vbios;
    brand_ = static_cast<brand_t>(brand);
    vendor_ = static_cast<vendor_t>(pci_info.pciDeviceId & 0xFFFF);
    subvendor_ = static_cast<subvendor_t>(pci_info.pciSubSystemId & 0xFFFF);
    arch_ = static_cast<arch_t>(arch);
    pci_bus_id_ = pci_info.busIdLegacy;
    bus_type_ = static_cast<bus_type_t>(bus_type);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_index() const noexcept
{
    return index_;
}



std::string NVMLpp::impl::NVML_Device_impl::get_name() const
{
    return name_;
}



NVMLpp::DeviceInfoProxy<NVMLpp::impl::NVML_Device_impl::brand_t> NVMLpp::impl::NVML_Device_impl::get_brand() const
{
    return DeviceInfoProxy {brand_};
}



NVMLpp::DeviceInfoProxy<NVMLpp::impl::NVML_Device_impl::vendor_t> NVMLpp::impl::NVML_Device_impl::get_vendor() const
{
    return DeviceInfoProxy {vendor_};
}



NVMLpp::DeviceInfoProxy<NVMLpp::impl::NVML_Device_impl::subvendor_t> NVMLpp::impl::NVML_Device_impl::get_subvendor() const
{
    return DeviceInfoProxy {subvendor_};
}



std::string NVMLpp::impl::NVML_Device_impl::get_uuid() const
{
    return uuid_;
}



std::string NVMLpp::impl::NVML_Device_impl::get_vbios_version() const
{
    return vbios_version_;
}



NVMLpp::DeviceInfoProxy<NVMLpp::impl::NVML_Device_impl::arch_t> NVMLpp::impl::NVML_Device_impl::get_arch() const
{
    return DeviceInfoProxy {arch_};
}



NVMLpp::DeviceInfoProxy<NVMLpp::impl::NVML_Device_impl::bus_type_t> NVMLpp::impl::NVML_Device_impl::get_bus_type() const
{
    return DeviceInfoProxy {bus_type_};
}



std::string NVMLpp::impl::NVML_Device_impl::get_pci_bus_id() const
{
    return pci_bus_id_;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_pci_bus_current_link_width() const
{
    unsigned link_width {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetCurrPcieLinkWidth(nvml_device_handle_, &link_width)
                );
    return link_width;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_pci_bus_max_link_width() const
{
    unsigned max_link_width {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetMaxPcieLinkWidth(nvml_device_handle_, &max_link_width)
                );
    return max_link_width;
}



size_t NVMLpp::impl::NVML_Device_impl::get_total_memory() const
{
    return std::get<memory_info_type::memory_total>(get_memory_info());
}



unsigned NVMLpp::impl::NVML_Device_impl::get_min_power_limit() const
{
    return std::get<power_limit_type::min_power_limit>(get_power_limits());
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_power_limit() const
{
    return std::get<power_limit_type::max_power_limit>(get_power_limits());
}



unsigned NVMLpp::impl::NVML_Device_impl::get_default_power_limit() const
{
    unsigned default_power_limit {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetPowerManagementDefaultLimit(nvml_device_handle_,
                                                                      &default_power_limit)
                );
    return default_power_limit;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_fans_count() const
{
    unsigned fans_count {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetNumFans(nvml_device_handle_, &fans_count)
                );
    return fans_count;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_min_fan_speed() const
{
    return std::get<fan_speed_limit_type::fan_min_speed>(get_fan_speed_limits());
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_fan_speed() const
{
    return std::get<fan_speed_limit_type::fan_max_speed>(get_fan_speed_limits());
}



unsigned NVMLpp::impl::NVML_Device_impl::get_temperature_threshold(temperature_threshold threshold) const
{
    using NVML_native::nvmlTemperatureThresholds_t;
    unsigned temp_threshold {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetTemperatureThreshold(nvml_device_handle_,
                                                               static_cast<nvmlTemperatureThresholds_t>(threshold),
                                                               &temp_threshold)
                );
    return temp_threshold;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_slowdown_temperature() const
{
    return get_temperature_threshold(temperature_threshold::slowdown);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_shutdown_temperature() const
{
    return get_temperature_threshold(temperature_threshold::shutdown);
}



size_t NVMLpp::impl::NVML_Device_impl::get_used_memory() const
{
    return std::get<memory_info_type::memory_used>(get_memory_info());
}



size_t NVMLpp::impl::NVML_Device_impl::get_free_memory() const
{
    return std::get<memory_info_type::memory_free>(get_memory_info());
}



size_t NVMLpp::impl::NVML_Device_impl::get_reserved_memory() const
{
    return std::get<memory_info_type::memory_used>(get_memory_info());
}



unsigned NVMLpp::impl::NVML_Device_impl::get_enforced_power_limit() const
{
    unsigned enforced_power_limit {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetEnforcedPowerLimit(nvml_device_handle_,
                                                             &enforced_power_limit)
                );
    return enforced_power_limit;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_current_power_limit() const
{
    unsigned current_power_limit {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetPowerManagementLimit(nvml_device_handle_,
                                                               &current_power_limit)
                );
    return current_power_limit;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_current_power_usage() const
{
    unsigned current_power_usage {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetPowerUsage(nvml_device_handle_, &current_power_usage)
                );
    return current_power_usage;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_current_temperature() const
{
    unsigned current_temperature {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetTemperature(nvml_device_handle_,
                                                      NVML_native::NVML_TEMPERATURE_GPU,
                                                      &current_temperature)
                );
    return current_temperature;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_current_fan_speed_level() const
{
    unsigned fan_speed_level {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetFanSpeed(nvml_device_handle_, &fan_speed_level)
                );
    return fan_speed_level;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_gpu_utilization() const
{
    return std::get<utilization_rates_type::gpu_utilization>(get_utilization_rates());
}



unsigned NVMLpp::impl::NVML_Device_impl::get_memory_utilization() const
{
    return std::get<utilization_rates_type::memory_utilization>(get_utilization_rates());
}



unsigned NVMLpp::impl::NVML_Device_impl::get_decoder_utilization() const
{
    unsigned decoder_utilization {0};
    unsigned sampling_period {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetDecoderUtilization(nvml_device_handle_, &decoder_utilization,
                                                             &sampling_period)
                );
    return decoder_utilization;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_encoder_utilization() const
{
    unsigned encoder_utilization {0};
    unsigned sampling_period {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetEncoderUtilization(nvml_device_handle_, &encoder_utilization,
                                                             &sampling_period)
                );
    return encoder_utilization;
}



NVMLpp::impl::NVML_Device_impl::performance_state NVMLpp::impl::NVML_Device_impl::get_performance_state() const
{
    NVML_native::nvmlPstates_t native_pstate {};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetPerformanceState(nvml_device_handle_, &native_pstate)
                );
    return static_cast<performance_state>(native_pstate);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_clock_graphics() const
{
    return get_current_clock_info(NVML_native::nvmlClockType_t::NVML_CLOCK_GRAPHICS);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_clock_video() const
{
    return get_current_clock_info(NVML_native::nvmlClockType_t::NVML_CLOCK_VIDEO);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_clock_sm() const
{
    return get_current_clock_info(NVML_native::nvmlClockType_t::NVML_CLOCK_SM);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_clock_memory() const
{
    return get_current_clock_info(NVML_native::nvmlClockType_t::NVML_CLOCK_MEM);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_clock_graphics() const
{
    return get_max_clock_info(NVML_native::nvmlClockType_t::NVML_CLOCK_GRAPHICS);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_clock_video() const
{
    return get_max_clock_info(NVML_native::nvmlClockType_t::NVML_CLOCK_VIDEO);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_clock_sm() const
{
    return get_max_clock_info(NVML_native::nvmlClockType_t::NVML_CLOCK_SM);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_clock_memory() const
{
    return get_max_clock_info(NVML_native::nvmlClockType_t::NVML_CLOCK_MEM);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_graphics_clock_of_current_pstate(performance_state pstate) const
{
    return get_max_clock_of_pstate(pstate, NVML_native::NVML_CLOCK_GRAPHICS);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_sm_clock_of_current_pstate(performance_state pstate) const
{
    return get_max_clock_of_pstate(pstate, NVML_native::NVML_CLOCK_SM);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_memory_clock_of_current_pstate(performance_state pstate) const
{
    return get_max_clock_of_pstate(pstate, NVML_native::NVML_CLOCK_MEM);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_video_clock_of_current_pstate(performance_state pstate) const
{
    return get_max_clock_of_pstate(pstate, NVML_native::NVML_CLOCK_VIDEO);
}



unsigned NVMLpp::impl::NVML_Device_impl::get_graphics_processess_count() const
{
    unsigned count {0};
    NVML_native::nvmlDeviceGetGraphicsRunningProcesses(nvml_device_handle_, &count, nullptr);
    return count;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_compute_processess_count() const
{
    unsigned count {0};
    NVML_native::nvmlDeviceGetComputeRunningProcesses(nvml_device_handle_, &count, nullptr);
    return count;
}



std::vector<NVML_native::nvmlProcessInfo_t> NVMLpp::impl::NVML_Device_impl::get_running_processes() const
{
    unsigned graphics_processes_count {get_graphics_processess_count()};
    unsigned compute_processes_count {get_compute_processess_count()};

    if (graphics_processes_count == 0 &&
            compute_processes_count == 0)
    {
        return {};
    }

    std::vector<NVML_native::nvmlProcessInfo_t> processes_list {};
    processes_list.reserve(graphics_processes_count);
    processes_list.resize(graphics_processes_count);

    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetGraphicsRunningProcesses(nvml_device_handle_,
                                                                   &graphics_processes_count,
                                                                   processes_list.data())
                );

    if (compute_processes_count == 0)
    {
        return processes_list;
    }

    std::vector<NVML_native::nvmlProcessInfo_t> compute_processes_list {};
    compute_processes_list.reserve(compute_processes_count);
    compute_processes_list.resize(compute_processes_count);

    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetComputeRunningProcesses(nvml_device_handle_,
                                                                  &compute_processes_count,
                                                                  compute_processes_list.data())
                );

    processes_list.insert(processes_list.end(),
                          std::make_move_iterator(compute_processes_list.begin()),
                          std::make_move_iterator(compute_processes_list.end()));

    return processes_list;
}



void NVMLpp::impl::NVML_Device_impl::set_power_limit(unsigned power_limit_watts)
{
    power_limit_watts *= 1000; // convert to milliwatts
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceSetPowerManagementLimit(nvml_device_handle_, power_limit_watts)
                );
}



void NVMLpp::impl::NVML_Device_impl::set_persistence_mode_enabled(bool enabled)
{
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceSetPersistenceMode(nvml_device_handle_,
                                                          static_cast<NVML_native::nvmlEnableState_t>(enabled))
                );
}



void NVMLpp::impl::NVML_Device_impl::set_fan_speed(unsigned fan_index, unsigned fan_speed_level)
{
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceSetFanSpeed_v2(nvml_device_handle_, fan_index, fan_speed_level)
                );
}



void NVMLpp::impl::NVML_Device_impl::set_default_fan_speed_policy(unsigned fan_index)
{
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceSetDefaultFanSpeed_v2(nvml_device_handle_, fan_index)
                );
}



void NVMLpp::impl::NVML_Device_impl::set_temperature_threshold(temperature_threshold threshold, int temperature)
{
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceSetTemperatureThreshold(nvml_device_handle_,
                                                               static_cast<NVML_native::nvmlTemperatureThresholds_t>(threshold),
                                                               &temperature)
                );
}

NVMLpp::impl::NVML_Device_impl::power_limits NVMLpp::impl::NVML_Device_impl::get_power_limits() const
{
    unsigned min_power_limit {0};
    unsigned max_power_limit {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetPowerManagementLimitConstraints(nvml_device_handle_, &min_power_limit,
                                                                          &max_power_limit)
                );
    return {min_power_limit, max_power_limit};
}



NVMLpp::impl::NVML_Device_impl::memory_info NVMLpp::impl::NVML_Device_impl::get_memory_info() const
{
    NVML_native::nvmlMemory_t memory_info {};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetMemoryInfo(nvml_device_handle_, &memory_info)
                );

    memory_info.total = memory_info.total;
    memory_info.used = memory_info.used;
    memory_info.free = memory_info.free;

    return {memory_info.total, memory_info.used, memory_info.free};
}



NVMLpp::impl::NVML_Device_impl::utilization_rates NVMLpp::impl::NVML_Device_impl::get_utilization_rates() const
{
    NVML_native::nvmlUtilization_t utilization_rates {};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetUtilizationRates(nvml_device_handle_, &utilization_rates)
                );
    return {utilization_rates.gpu, utilization_rates.memory};
}



NVMLpp::impl::NVML_Device_impl::fan_speed_limits NVMLpp::impl::NVML_Device_impl::get_fan_speed_limits() const
{
    unsigned min_fan_speed {0};
    unsigned max_fan_speed {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetMinMaxFanSpeed(nvml_device_handle_, &min_fan_speed, &max_fan_speed)
                );
    return {min_fan_speed, max_fan_speed};
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_clock_of_pstate(performance_state pstate,
                                                                 NVML_native::nvmlClockType_t clock_type) const
{
    unsigned min_clock_value {0};
    unsigned max_clock_value {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetMinMaxClockOfPState(nvml_device_handle_, clock_type,
                                                              static_cast<NVML_native::nvmlPstates_t>(pstate),
                                                              &min_clock_value, &max_clock_value)
                );
    return max_clock_value;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_current_clock_info(NVML_native::nvmlClockType_t clock_type) const
{
    unsigned current_clock {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetClockInfo(nvml_device_handle_, clock_type, &current_clock)
                );
    return current_clock;
}



unsigned NVMLpp::impl::NVML_Device_impl::get_max_clock_info(NVML_native::nvmlClockType_t clock_type) const
{
    unsigned max_clock {0};
    NVMLPP_MAKE_NATIVE_CALL(
                NVML_native::nvmlDeviceGetMaxClockInfo(nvml_device_handle_, clock_type, &max_clock)
                );
    return max_clock;
}
