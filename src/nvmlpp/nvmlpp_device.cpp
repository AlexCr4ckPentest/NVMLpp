#include <algorithm>

#include "nvmlpp/nvmlpp_device.hpp"

void NVMLpp::NVML_device::device_deleter::operator()(impl::NVML_Device_impl* const ptr) const
{ delete ptr; }



NVMLpp::NVML_device::NVML_device(NVML_device&& other) noexcept
{
    std::swap(nvml_device_impl_, other.nvml_device_impl_);
}

NVMLpp::NVML_device& NVMLpp::NVML_device::operator=(NVML_device&& other) noexcept
{
    std::swap(*this, other);
    return *this;
}

NVMLpp::NVML_device NVMLpp::NVML_device::from_index(unsigned device_index)
{
    return NVML_device {device_index};
}

NVMLpp::NVML_device NVMLpp::NVML_device::from_uuid(std::string_view device_uuid)
{
    return NVML_device {device_uuid};
}

unsigned NVMLpp::NVML_device::get_index() const noexcept
{
    return nvml_device_impl_->get_index();
}

std::string NVMLpp::NVML_device::get_name() const
{
    return nvml_device_impl_->get_name();
}

NVMLpp::DeviceInfoProxy<NVMLpp::NVML_device::brand_t> NVMLpp::NVML_device::get_brand() const
{
    return nvml_device_impl_->get_brand();
}

NVMLpp::DeviceInfoProxy<NVMLpp::NVML_device::vendor_t> NVMLpp::NVML_device::get_vendor() const
{
    return nvml_device_impl_->get_vendor();
}

NVMLpp::DeviceInfoProxy<NVMLpp::NVML_device::subvendor_t> NVMLpp::NVML_device::get_subvendor() const
{
    return nvml_device_impl_->get_subvendor();
}

std::string NVMLpp::NVML_device::get_uuid() const
{
    return nvml_device_impl_->get_uuid();
}

std::string NVMLpp::NVML_device::get_vbios_version() const
{
    return nvml_device_impl_->get_vbios_version();
}

NVMLpp::DeviceInfoProxy<NVMLpp::NVML_device::arch_t> NVMLpp::NVML_device::get_arch() const
{
    return nvml_device_impl_->get_arch();
}

NVMLpp::DeviceInfoProxy<NVMLpp::NVML_device::bus_type_t> NVMLpp::NVML_device::get_bus_type() const
{
    return nvml_device_impl_->get_bus_type();
}

std::string NVMLpp::NVML_device::get_pci_bus_id() const
{
    return nvml_device_impl_->get_pci_bus_id();
}

unsigned NVMLpp::NVML_device::get_pci_bus_current_link_width() const
{
    return nvml_device_impl_->get_pci_bus_current_link_width();
}

unsigned NVMLpp::NVML_device::get_pci_bus_max_link_width() const
{
    return nvml_device_impl_->get_pci_bus_max_link_width();
}

size_t NVMLpp::NVML_device::get_total_memory() const
{
    return nvml_device_impl_->get_total_memory();
}

unsigned NVMLpp::NVML_device::get_min_power_limit() const
{
    return nvml_device_impl_->get_min_power_limit();
}

unsigned NVMLpp::NVML_device::get_max_power_limit() const
{
    return nvml_device_impl_->get_max_power_limit();
}

unsigned NVMLpp::NVML_device::get_default_power_limit() const
{
    return nvml_device_impl_->get_default_power_limit();
}

unsigned NVMLpp::NVML_device::get_fans_count() const
{
    return nvml_device_impl_->get_fans_count();
}

unsigned NVMLpp::NVML_device::get_min_fan_speed() const
{
    return nvml_device_impl_->get_min_fan_speed();
}

unsigned NVMLpp::NVML_device::get_max_fan_speed() const
{
    return nvml_device_impl_->get_max_fan_speed();
}

unsigned NVMLpp::NVML_device::get_temperature_threshold(temperature_threshold threshold) const
{
    return nvml_device_impl_->get_temperature_threshold(threshold);
}

unsigned NVMLpp::NVML_device::get_slowdown_temperature() const
{
    return nvml_device_impl_->get_slowdown_temperature();
}

unsigned NVMLpp::NVML_device::get_shutdown_temperature() const
{
    return nvml_device_impl_->get_shutdown_temperature();
}

size_t NVMLpp::NVML_device::get_used_memory() const
{
    return nvml_device_impl_->get_used_memory();
}

size_t NVMLpp::NVML_device::get_free_memory() const
{
    return nvml_device_impl_->get_free_memory();
}

size_t NVMLpp::NVML_device::get_reserved_memory() const
{
    return nvml_device_impl_->get_reserved_memory();
}

unsigned NVMLpp::NVML_device::get_enforced_power_limit() const
{
    return nvml_device_impl_->get_enforced_power_limit();
}

unsigned NVMLpp::NVML_device::get_current_power_limit() const
{
    return nvml_device_impl_->get_current_power_limit();
}

unsigned NVMLpp::NVML_device::get_current_power_usage() const
{
    return nvml_device_impl_->get_current_power_usage();
}

unsigned NVMLpp::NVML_device::get_current_temperature() const
{
    return nvml_device_impl_->get_current_temperature();
}

unsigned NVMLpp::NVML_device::get_current_fan_speed_level() const
{
    return nvml_device_impl_->get_current_fan_speed_level();
}

unsigned NVMLpp::NVML_device::get_gpu_utilization() const
{
    return nvml_device_impl_->get_gpu_utilization();
}

unsigned NVMLpp::NVML_device::get_memory_utilization() const
{
    return nvml_device_impl_->get_memory_utilization();
}

unsigned NVMLpp::NVML_device::get_decoder_utilization() const
{
    return nvml_device_impl_->get_decoder_utilization();
}

unsigned NVMLpp::NVML_device::get_encoder_utilization() const
{
    return nvml_device_impl_->get_encoder_utilization();
}

NVMLpp::NVML_device::performance_state NVMLpp::NVML_device::get_performance_state() const
{
    return nvml_device_impl_->get_performance_state();
}

unsigned NVMLpp::NVML_device::get_clock_graphics() const
{
    return nvml_device_impl_->get_clock_graphics();
}

unsigned NVMLpp::NVML_device::get_clock_video() const
{
    return nvml_device_impl_->get_clock_video();
}

unsigned NVMLpp::NVML_device::get_clock_sm() const
{
    return nvml_device_impl_->get_clock_sm();
}

unsigned NVMLpp::NVML_device::get_clock_memory() const
{
    return nvml_device_impl_->get_clock_memory();
}

unsigned NVMLpp::NVML_device::get_max_clock_graphics() const
{
    return nvml_device_impl_->get_max_clock_graphics();
}

unsigned NVMLpp::NVML_device::get_max_clock_video() const
{
    return nvml_device_impl_->get_max_clock_video();
}

unsigned NVMLpp::NVML_device::get_max_clock_sm() const
{
    return nvml_device_impl_->get_max_clock_sm();
}

unsigned NVMLpp::NVML_device::get_max_clock_memory() const
{
    return nvml_device_impl_->get_max_clock_memory();
}

unsigned NVMLpp::NVML_device::get_max_graphics_clock_of_current_pstate(performance_state pstate) const
{
    return nvml_device_impl_->get_max_graphics_clock_of_current_pstate(pstate);
}

unsigned NVMLpp::NVML_device::get_max_sm_clock_of_current_pstate(performance_state pstate) const
{
    return nvml_device_impl_->get_max_sm_clock_of_current_pstate(pstate);
}

unsigned NVMLpp::NVML_device::get_max_memory_clock_of_current_pstate(performance_state pstate) const
{
    return nvml_device_impl_->get_max_memory_clock_of_current_pstate(pstate);
}

unsigned NVMLpp::NVML_device::get_max_video_clock_of_current_pstate(performance_state pstate) const
{
    return nvml_device_impl_->get_max_video_clock_of_current_pstate(pstate);
}

unsigned NVMLpp::NVML_device::get_graphics_processess_count() const
{
    return nvml_device_impl_->get_graphics_processess_count();
}

unsigned NVMLpp::NVML_device::get_compute_processess_count() const
{
    return nvml_device_impl_->get_compute_processess_count();
}

std::vector<NVML_native::nvmlProcessInfo_t> NVMLpp::NVML_device::get_running_processes() const
{
    return nvml_device_impl_->get_running_processes();
}

void NVMLpp::NVML_device::set_power_limit(unsigned power_limit_watts)
{
    nvml_device_impl_->set_power_limit(power_limit_watts);
}

void NVMLpp::NVML_device::set_persistence_mode_enabled(bool enabled)
{
    nvml_device_impl_->set_persistence_mode_enabled(enabled);
}

void NVMLpp::NVML_device::set_fan_speed(unsigned fan_index, unsigned fan_speed_level)
{
    nvml_device_impl_->set_fan_speed(fan_index, fan_speed_level);
}

void NVMLpp::NVML_device::set_default_fan_speed_policy(unsigned fan_index)
{
    nvml_device_impl_->set_default_fan_speed_policy(fan_index);
}

void NVMLpp::NVML_device::set_temperature_threshold(temperature_threshold threshold, int temperature)
{
    nvml_device_impl_->set_temperature_threshold(threshold, temperature);
}

NVMLpp::NVML_device::NVML_device(unsigned device_index)
    : nvml_device_impl_ {new impl::NVML_Device_impl {device_index}, device_deleter{}}
{ }

NVMLpp::NVML_device::NVML_device(std::string_view device_uuid)
    : nvml_device_impl_ {new impl::NVML_Device_impl {device_uuid}, device_deleter{}}
{ }
