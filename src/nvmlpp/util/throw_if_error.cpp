#include "nvmlpp/util/throw_if_error.hpp"
#include "nvmlpp/util/nvmlpp_errors.hpp"

#include <string>
#include <stdexcept>

void NVMLpp::utils::throw_if_error(const char* func_name, NVML_native::nvmlReturn_t err_code)
{
    if (err_code != NVML_native::NVML_SUCCESS)
    {
        const std::string err_msg {std::string{func_name} + ": " + std::string{nvmlErrorString(err_code)}};
        switch (err_code)
        {
        using namespace NVML_native;
        case NVML_ERROR_UNINITIALIZED:              throw errors::error_uninitialized {err_msg};            break;
        case NVML_ERROR_INVALID_ARGUMENT:           throw errors::error_invalid_argument {err_msg};         break;
        case NVML_ERROR_NOT_SUPPORTED:              throw errors::error_not_supported {err_msg};            break;
        case NVML_ERROR_NO_PERMISSION:              throw errors::error_no_permission {err_msg};            break;
        case NVML_ERROR_ALREADY_INITIALIZED:        throw errors::error_already_initialized {err_msg};      break;
        case NVML_ERROR_NOT_FOUND:                  throw errors::error_not_found {err_msg};                break;
        case NVML_ERROR_INSUFFICIENT_SIZE:          throw errors::error_insuffcient_size {err_msg};         break;
        case NVML_ERROR_INSUFFICIENT_POWER:         throw errors::error_insuffcient_power {err_msg};        break;
        case NVML_ERROR_DRIVER_NOT_LOADED:          throw errors::error_driver_not_loaded {err_msg};        break;
        case NVML_ERROR_TIMEOUT:                    throw errors::error_timeout {err_msg};                  break;
        case NVML_ERROR_IRQ_ISSUE:                  throw errors::error_irq_issue {err_msg};                break;
        case NVML_ERROR_LIBRARY_NOT_FOUND:          throw errors::error_library_not_found {err_msg};        break;
        case NVML_ERROR_FUNCTION_NOT_FOUND:         throw errors::error_function_not_found {err_msg};       break;
        case NVML_ERROR_CORRUPTED_INFOROM:          throw errors::error_corrupted_inforom {err_msg};        break;
        case NVML_ERROR_GPU_IS_LOST:                throw errors::error_gpu_is_lost {err_msg};              break;
        case NVML_ERROR_RESET_REQUIRED:             throw errors::error_reset_required {err_msg};           break;
        case NVML_ERROR_OPERATING_SYSTEM:           throw errors::error_operating_system {err_msg};         break;
        case NVML_ERROR_LIB_RM_VERSION_MISMATCH:    throw errors::error_lib_rm_version_mismatch {err_msg};  break;
        case NVML_ERROR_IN_USE:                     throw errors::error_in_use {err_msg};                   break;
        case NVML_ERROR_MEMORY:                     throw errors::error_memory {err_msg};                   break;
        case NVML_ERROR_NO_DATA:                    throw errors::error_no_data {err_msg};                  break;
        case NVML_ERROR_VGPU_ECC_NOT_SUPPORTED:     throw errors::error_vgpu_ecc_not_supported {err_msg};   break;
        case NVML_ERROR_INSUFFICIENT_RESOURCES:     throw errors::error_insuffcient_resources {err_msg};    break;
        case NVML_ERROR_FREQ_NOT_SUPPORTED:         throw errors::error_freq_not_supported {err_msg};       break;
        case NVML_ERROR_UNKNOWN: default:           throw errors::error_unknown {err_msg};                  break;
        }
    }
}
