#pragma once

#include <string>
#include <vector>

#include "nvmlpp/nvmlpp_device.hpp"
#include "nvmlpp/nvmlpp_unit.hpp"

namespace NVMLpp
{
class Session
{
public:
    const std::string& get_NVML_version() const;
    const std::string& get_system_driver_version() const;
    int get_cuda_version() const;
    const std::vector<NVML_device>& get_available_devices();
    const std::vector<NVML_unit>& get_available_units();

    static Session& instance();

private:
    Session();
    ~Session();

    template<typename T>
    void check_and_update(std::vector<T>& target, unsigned device_count) const
    {
        if (target.empty() || target.size() != device_count)
        {
            std::vector<T> tmp {};
            tmp.reserve(device_count);
            for (unsigned i {0}; i < device_count; i++)
            {
                tmp.emplace_back(T::from_index(i));
            }
            std::swap(target, tmp);
        }
    }

    std::vector<NVML_device> nvml_devices_;
    std::vector<NVML_unit> nvml_units_;
};
} // namespace NVMLpp
