#pragma once

#include <memory>

#include "nvmlpp/impl/nvmlpp_device_impl.hpp"

namespace NVMLpp
{

class NVML_device final
{
    struct device_deleter final
    { void operator()(impl::NVML_Device_impl* const ptr) const; };

public:
    using arch_t                    = impl::NVML_Device_impl::arch_t;
    using brand_t                   = impl::NVML_Device_impl::brand_t;
    using bus_type_t                = impl::NVML_Device_impl::bus_type_t;
    using vendor_t                  = impl::NVML_Device_impl::vendor_t;
    using subvendor_t               = impl::NVML_Device_impl::subvendor_t;
    using performance_state         = impl::NVML_Device_impl::performance_state;
    using temperature_threshold     = impl::NVML_Device_impl::temperature_threshold;

    NVML_device(const NVML_device&) = delete;
    NVML_device& operator=(const NVML_device&) = delete;

    NVML_device(NVML_device&&) noexcept;
    NVML_device& operator=(NVML_device&&) noexcept;
    ~NVML_device() = default;

    static NVML_device from_index(unsigned device_index);
    static NVML_device from_uuid(std::string_view device_uuid);

    unsigned get_index() const noexcept;
    std::string get_name() const;
    DeviceInfoProxy<brand_t> get_brand() const;
    DeviceInfoProxy<vendor_t> get_vendor() const;
    DeviceInfoProxy<subvendor_t> get_subvendor() const;
    std::string get_uuid() const;
    std::string get_vbios_version() const;
    DeviceInfoProxy<arch_t> get_arch() const;
    DeviceInfoProxy<bus_type_t> get_bus_type() const;
    std::string get_pci_bus_id() const;
    unsigned get_pci_bus_current_link_width() const;
    unsigned get_pci_bus_max_link_width() const;
    size_t get_total_memory() const;
    unsigned get_min_power_limit() const;
    unsigned get_max_power_limit() const;
    unsigned get_default_power_limit() const;
    unsigned get_fans_count() const;
    unsigned get_min_fan_speed() const;
    unsigned get_max_fan_speed() const;
    unsigned get_temperature_threshold(temperature_threshold threshold) const;
    unsigned get_slowdown_temperature() const;
    unsigned get_shutdown_temperature() const;

    size_t get_used_memory() const;
    size_t get_free_memory() const;
    size_t get_reserved_memory() const;
    unsigned get_enforced_power_limit() const;
    unsigned get_current_power_limit() const;
    unsigned get_current_power_usage() const;
    unsigned get_current_temperature() const;
    unsigned get_current_fan_speed_level() const;
    unsigned get_gpu_utilization() const;
    unsigned get_memory_utilization() const;
    unsigned get_decoder_utilization() const;
    unsigned get_encoder_utilization() const;
    performance_state get_performance_state() const;
    unsigned get_clock_graphics() const;
    unsigned get_clock_video() const;
    unsigned get_clock_sm() const;
    unsigned get_clock_memory() const;
    unsigned get_max_clock_graphics() const;
    unsigned get_max_clock_video() const;
    unsigned get_max_clock_sm() const;
    unsigned get_max_clock_memory() const;
    unsigned get_max_graphics_clock_of_current_pstate(performance_state pstate) const;
    unsigned get_max_sm_clock_of_current_pstate(performance_state pstate) const;
    unsigned get_max_memory_clock_of_current_pstate(performance_state pstate) const;
    unsigned get_max_video_clock_of_current_pstate(performance_state pstate) const;

    unsigned get_graphics_processess_count() const;
    unsigned get_compute_processess_count() const;
    std::vector<NVML_native::nvmlProcessInfo_t> get_running_processes() const;

    void set_power_limit(unsigned power_limit_watts); // requires run as root
    void set_persistence_mode_enabled(bool enabled); // requires run as root

    void set_fan_speed(unsigned fan_index, unsigned fan_speed_level);
    void set_default_fan_speed_policy(unsigned fan_index);
    void set_temperature_threshold(temperature_threshold threshold, int temperature);

private:
    std::unique_ptr<impl::NVML_Device_impl, device_deleter> nvml_device_impl_;

    explicit NVML_device(unsigned device_index);
    explicit NVML_device(std::string_view device_uuid);
};

} // namespace NVMLpp
