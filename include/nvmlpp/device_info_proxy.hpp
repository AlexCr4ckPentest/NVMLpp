#pragma once

namespace NVMLpp
{

template<typename T>
DeviceInfoProxy(T) -> DeviceInfoProxy<T>;

template<>
class DeviceInfoProxy<impl::NVML_Device_impl::arch_t>
{
    impl::NVML_Device_impl::arch_t value_;
    std::string arch_to_string(impl::NVML_Device_impl::arch_t arch) const
    {
        switch (arch)
        { using arch = impl::NVML_Device_impl::arch_t;
        case arch::kepler: return "Kepler"; break;
        case arch::maxwell: return "Maxwell"; break;
        case arch::pascal: return "Pascal"; break;
        case arch::volta: return "Volta"; break;
        case arch::turing: return "Turing"; break;
        case arch::ampere: return "Ampere"; break;
        case arch::unknown:
        default:
            return "Unknown";
            break;
        }
    }

public:
    DeviceInfoProxy(impl::NVML_Device_impl::arch_t value) noexcept : value_ {value} {}

    operator impl::NVML_Device_impl::arch_t() const noexcept { return value_; }
    std::string to_string() const { return arch_to_string(value_); }
};

template<>
class DeviceInfoProxy<impl::NVML_Device_impl::brand_t>
{
    impl::NVML_Device_impl::brand_t value_;
    std::string brand_to_string(impl::NVML_Device_impl::brand_t brand_type) const
    {
        switch (brand_type)
        { using brand = impl::NVML_Device_impl::brand_t;
        case brand::quadro: return "Quadro"; break;
        case brand::tesla: return "Tesla"; break;
        case brand::nvs: return "NVS"; break;
        case brand::grid: return "Grid"; break;
        case brand::geforce: return "GeForce"; break;
        case brand::titan: return "Titan"; break;
        case brand::nvidia_vapps: return "NVIDIA VAPPS"; break;
        case brand::nvidia_vpc: return "NVIDIA VPC"; break;
        case brand::nvidia_vcs: return "NVIDIA VCS"; break;
        case brand::nvidia_vws: return "NVIDIA VWS"; break;
        case brand::nvidia_cloud_gaming: return "NVIDIA Cloud Gaming"; break;
        case brand::quadro_rtx: return "Quadro RTX"; break;
        case brand::nvidia_rtx: return "NVIDIA RTX"; break;
        case brand::nvidia: return "NVIDIA"; break;
        case brand::unknown:
        default:
            return "Unknown";
        }
    }

public:
    DeviceInfoProxy(impl::NVML_Device_impl::brand_t value) noexcept : value_ {value} {}

    operator impl::NVML_Device_impl::brand_t() const noexcept { return value_; }
    std::string to_string() const { return brand_to_string(value_); }
};

template<>
class DeviceInfoProxy<impl::NVML_Device_impl::bus_type_t>
{
    impl::NVML_Device_impl::bus_type_t value_;
    std::string bus_type_to_string(impl::NVML_Device_impl::bus_type_t bus_type) const
    {
        switch (bus_type)
        { using type = impl::NVML_Device_impl::bus_type_t;
        case type::pci: return "PCI"; break;
        case type::pcie: return "PCIe"; break;
        case type::fpci: return "FPCI"; break;
        case type::agp: return "AGP"; break;
        case type::unknown:
        default:
            return "Unknown";
            break;
        }
    }

public:
    DeviceInfoProxy(impl::NVML_Device_impl::bus_type_t value) noexcept : value_ {value} {}

    operator impl::NVML_Device_impl::bus_type_t() const noexcept { return value_; }
    std::string to_string() const { return bus_type_to_string(value_); }
};

template<>
class DeviceInfoProxy<impl::NVML_Device_impl::vendor_t>
{
    impl::NVML_Device_impl::vendor_t value_;
    std::string vendor_to_string(impl::NVML_Device_impl::vendor_t vendor_id) const
    {
        if (vendor_id == impl::NVML_Device_impl::vendor_t::nvidia)
        {
            return "Nvidia";
        }
        return "Unknown";
    }

public:
    DeviceInfoProxy(impl::NVML_Device_impl::vendor_t value) noexcept : value_ {value} {}
    operator impl::NVML_Device_impl::vendor_t() const noexcept { return value_; }
    std::string to_string() const { return vendor_to_string(value_); }
};

template<>
class DeviceInfoProxy<impl::NVML_Device_impl::subvendor_t>
{
    impl::NVML_Device_impl::subvendor_t value_;
    std::string subvendor_to_string(impl::NVML_Device_impl::subvendor_t subvendor_id) const
    {
        switch (subvendor_id)
        { using subvendor = impl::NVML_Device_impl::subvendor_t;
        case subvendor::asus: return "Asus"; break;
        case subvendor::elsa: return "Elsa"; break;
        case subvendor::leadtek: return "Leadtek"; break;
        case subvendor::gainward: return "Gainward"; break;
        case subvendor::gigabyte: return "Gigabyte"; break;
        case subvendor::msi: return "MSI"; break;
        case subvendor::pny_1: return "PNY"; break;
        case subvendor::palit: return "Palit"; break;
        case subvendor::xfx: return "XFX"; break;
        case subvendor::club3d: return "Club3D"; break;
        case subvendor::pny_2: return "PNY"; break;
        case subvendor::zotac: return "Zotac"; break;
        case subvendor::bfg: return "BFG"; break;
        case subvendor::pov: return "PoV"; break;
        case subvendor::galax_kfa2: return "Galax (KFA2)"; break;
        case subvendor::evga: return "EVGA"; break;
        case subvendor::colorful: return "Colorful"; break;
        default:
            return "Unknown";
            break;
        }
    }

public:
    DeviceInfoProxy(impl::NVML_Device_impl::subvendor_t value) noexcept : value_ {value} {}
    operator impl::NVML_Device_impl::subvendor_t() const noexcept { return value_; }
    std::string to_string() const { return subvendor_to_string(value_); }
};

} // namespace NVMLpp
