#pragma once

#include "nvmlpp/nvml_native.hpp"

namespace NVMLpp
{
class NVML_unit
{
    NVML_unit(unsigned unit_index);
    NVML_unit(NVML_native::nvmlUnit_t native_nvml_unit) noexcept;

public:
    NVML_unit(const NVML_unit& other) = default;
    NVML_unit& operator=(const NVML_unit&) = default;
    NVML_unit(NVML_unit&& other) noexcept;
    NVML_unit& operator=(NVML_unit&& other) noexcept;

    static NVML_unit from_index(unsigned unit_index) noexcept;
    static NVML_unit from_native_handle(NVML_native::nvmlUnit_t native_nvml_unit) noexcept;

private:
    NVML_native::nvmlUnit_t nvml_unit_handle_;
};
}
