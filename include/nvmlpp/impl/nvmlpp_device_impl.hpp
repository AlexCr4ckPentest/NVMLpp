#pragma once

#include <string>
#include <tuple>
#include <vector>

#include "nvmlpp/nvml_native.hpp"

namespace NVMLpp
{
template<typename>
class DeviceInfoProxy {};
} // namespace NVMLpp

namespace NVMLpp::impl
{

class NVML_Device_impl final
{
    enum power_limit_type { min_power_limit, max_power_limit };
    enum memory_info_type { memory_total, memory_used, memory_free };
    enum utilization_rates_type { gpu_utilization, memory_utilization };
    enum fan_speed_limit_type { fan_min_speed, fan_max_speed };
    using power_limits = std::tuple<unsigned, unsigned>;
    using memory_info = std::tuple<std::size_t, std::size_t, size_t>;
    using utilization_rates = std::tuple<unsigned, unsigned>;
    using fan_speed_limits = std::tuple<unsigned, unsigned>;

public:
    enum class performance_state
    {
        _0 = NVML_native::NVML_PSTATE_0, // max
        _1 = NVML_native::NVML_PSTATE_1,
        _2 = NVML_native::NVML_PSTATE_2,
        _3 = NVML_native::NVML_PSTATE_3,
        _4 = NVML_native::NVML_PSTATE_4,
        _5 = NVML_native::NVML_PSTATE_5,
        _6 = NVML_native::NVML_PSTATE_6,
        _7 = NVML_native::NVML_PSTATE_7,
        _8 = NVML_native::NVML_PSTATE_8,
        _9 = NVML_native::NVML_PSTATE_9,
        _10 = NVML_native::NVML_PSTATE_10,
        _11 = NVML_native::NVML_PSTATE_11,
        _12 = NVML_native::NVML_PSTATE_12,
        _13 = NVML_native::NVML_PSTATE_13,
        _14 = NVML_native::NVML_PSTATE_14,
        _15 = NVML_native::NVML_PSTATE_15, // min
        unknown = NVML_native::NVML_PSTATE_UNKNOWN,
    };

    enum class persistence_mode { off, on };
    enum class temperature_threshold { shutdown, slowdown, mem_max, gpu_max, acoustic_min, acoustic_curr, acoustic_max };

    enum class arch_t : unsigned
    {
        kepler = NVML_DEVICE_ARCH_KEPLER,
        maxwell = NVML_DEVICE_ARCH_MAXWELL,
        pascal = NVML_DEVICE_ARCH_PASCAL,
        volta = NVML_DEVICE_ARCH_VOLTA,
        turing = NVML_DEVICE_ARCH_TURING,
        ampere = NVML_DEVICE_ARCH_AMPERE,
        unknown = NVML_DEVICE_ARCH_UNKNOWN,
    };

    enum class brand_t : unsigned
    {
        quadro = NVML_native::NVML_BRAND_QUADRO,
        tesla = NVML_native::NVML_BRAND_TESLA,
        nvs = NVML_native::NVML_BRAND_NVS,
        grid = NVML_native::NVML_BRAND_GRID,
        geforce = NVML_native::NVML_BRAND_GEFORCE,
        titan = NVML_native::NVML_BRAND_TITAN,
        nvidia_vapps = NVML_native::NVML_BRAND_NVIDIA_VAPPS,
        nvidia_vpc = NVML_native::NVML_BRAND_NVIDIA_VPC,
        nvidia_vcs = NVML_native::NVML_BRAND_NVIDIA_VCS,
        nvidia_vws = NVML_native::NVML_BRAND_NVIDIA_VWS,
        nvidia_cloud_gaming = NVML_native::NVML_BRAND_NVIDIA_CLOUD_GAMING,
        quadro_rtx = NVML_native::NVML_BRAND_QUADRO_RTX,
        nvidia_rtx = NVML_native::NVML_BRAND_NVIDIA_RTX,
        nvidia = NVML_native::NVML_BRAND_NVIDIA,
        unknown = NVML_native::NVML_BRAND_UNKNOWN
    };

    enum class vendor_t
    {
        nvidia = 0x10DE,
        unknown = 0x0
    };

    enum class bus_type_t
    {
        pci = NVML_BUS_TYPE_PCI,
        pcie = NVML_BUS_TYPE_PCIE,
        fpci = NVML_BUS_TYPE_FPCI,
        agp = NVML_BUS_TYPE_AGP,
        unknown = NVML_BUS_TYPE_UNKNOWN
    };

    enum class subvendor_t
    {
        asus = 0x1043,
        elsa = 0x1048,
        leadtek = 0x107D,
        gainward = 0x10B0,
        gigabyte = 0x1458,
        msi = 0x1462,
        pny_1 = 0x154B,
        pny_2 = 0x196E,
        palit = 0x1569,
        xfx = 0x1682,
        club3d = 0x196D,
        zotac = 0x19DA,
        bfg = 0x19F1,
        pov = 0x1ACC,
        galax_kfa2 = 0x1B4C,
        evga = 0x3842,
        colorful = 0x7377,
        unknown = 0x0
    };

    NVML_Device_impl(unsigned device_index);
    NVML_Device_impl(std::string_view device_uuid);
    NVML_Device_impl(NVML_Device_impl&& other) noexcept;
    NVML_Device_impl& operator=(NVML_Device_impl&& other) noexcept;

    unsigned get_index() const noexcept;
    std::string get_name() const;
    DeviceInfoProxy<brand_t> get_brand() const;
    DeviceInfoProxy<vendor_t> get_vendor() const;
    DeviceInfoProxy<subvendor_t> get_subvendor() const;
    std::string get_uuid() const;
    std::string get_vbios_version() const;
    DeviceInfoProxy<arch_t> get_arch() const;
    DeviceInfoProxy<bus_type_t> get_bus_type() const;
    std::string get_pci_bus_id() const;
    unsigned get_pci_bus_current_link_width() const;
    unsigned get_pci_bus_max_link_width() const;
    size_t get_total_memory() const;
    unsigned get_min_power_limit() const;
    unsigned get_max_power_limit() const;
    unsigned get_default_power_limit() const;
    unsigned get_fans_count() const;
    unsigned get_min_fan_speed() const;
    unsigned get_max_fan_speed() const;
    unsigned get_temperature_threshold(temperature_threshold threshold) const;
    unsigned get_slowdown_temperature() const;
    unsigned get_shutdown_temperature() const;

    size_t get_used_memory() const;
    size_t get_free_memory() const;
    size_t get_reserved_memory() const;
    unsigned get_enforced_power_limit() const;
    unsigned get_current_power_limit() const;
    unsigned get_current_power_usage() const;
    unsigned get_current_temperature() const;
    unsigned get_current_fan_speed_level() const;
    unsigned get_gpu_utilization() const;
    unsigned get_memory_utilization() const;
    unsigned get_decoder_utilization() const;
    unsigned get_encoder_utilization() const;
    performance_state get_performance_state() const;
    unsigned get_clock_graphics() const;
    unsigned get_clock_video() const;
    unsigned get_clock_sm() const;
    unsigned get_clock_memory() const;
    unsigned get_max_clock_graphics() const;
    unsigned get_max_clock_video() const;
    unsigned get_max_clock_sm() const;
    unsigned get_max_clock_memory() const;
    unsigned get_max_graphics_clock_of_current_pstate(performance_state pstate) const;
    unsigned get_max_sm_clock_of_current_pstate(performance_state pstate) const;
    unsigned get_max_memory_clock_of_current_pstate(performance_state pstate) const;
    unsigned get_max_video_clock_of_current_pstate(performance_state pstate) const;

    unsigned get_graphics_processess_count() const;
    unsigned get_compute_processess_count() const;
    std::vector<NVML_native::nvmlProcessInfo_t> get_running_processes() const;

    void set_power_limit(unsigned power_limit_watts); // requires run as root
    void set_persistence_mode_enabled(bool enabled); // requires run as root

    void set_fan_speed(unsigned fan_index, unsigned fan_speed_level);
    void set_default_fan_speed_policy(unsigned fan_index);
    void set_temperature_threshold(temperature_threshold threshold, int temperature);

private:
    NVML_native::nvmlDevice_t nvml_device_handle_;

    unsigned index_;
    std::string name_;
    brand_t brand_;
    vendor_t vendor_;
    subvendor_t subvendor_;
    std::string uuid_;
    std::string vbios_version_;
    arch_t arch_;
    std::string pci_bus_id_;
    bus_type_t bus_type_;

    void initialize_static_info();
    power_limits get_power_limits() const;
    memory_info get_memory_info() const;
    utilization_rates get_utilization_rates() const;
    fan_speed_limits get_fan_speed_limits() const;
    unsigned get_max_clock_of_pstate(performance_state pstate, NVML_native::nvmlClockType_t clock_type) const;
    unsigned get_current_clock_info(NVML_native::nvmlClockType_t clock_type) const;
    unsigned get_max_clock_info(NVML_native::nvmlClockType_t clock_type) const;
};

} // namespace NVMLpp::impl

#include "nvmlpp/device_info_proxy.hpp"
