#pragma once

#include <string>
#include <stdexcept>

namespace NVMLpp::errors
{
using error = std::runtime_error;
struct error_uninitialized              : public error { error_uninitialized(const std::string& msg) : error {msg} {} };
struct error_invalid_argument           : public error { error_invalid_argument(const std::string& msg) : error {msg} {} };
struct error_not_supported              : public error { error_not_supported(const std::string& msg) : error {msg} {} };
struct error_no_permission              : public error { error_no_permission(const std::string& msg) : error {msg} {} };
struct error_already_initialized        : public error { error_already_initialized(const std::string& msg) : error {msg} {} };
struct error_not_found                  : public error { error_not_found(const std::string& msg) : error {msg} {} };
struct error_insuffcient_size           : public error { error_insuffcient_size(const std::string& msg) : error {msg} {} };
struct error_insuffcient_power          : public error { error_insuffcient_power(const std::string& msg) : error {msg} {} };
struct error_driver_not_loaded          : public error { error_driver_not_loaded(const std::string& msg) : error {msg} {} };
struct error_timeout                    : public error { error_timeout(const std::string& msg) : error {msg} {} };
struct error_irq_issue                  : public error { error_irq_issue(const std::string& msg) : error {msg} {} };
struct error_library_not_found          : public error { error_library_not_found(const std::string& msg) : error {msg} {} };
struct error_function_not_found         : public error { error_function_not_found(const std::string& msg) : error {msg} {} };
struct error_corrupted_inforom          : public error { error_corrupted_inforom(const std::string& msg) : error {msg} {} };
struct error_gpu_is_lost                : public error { error_gpu_is_lost(const std::string& msg) : error {msg} {} };
struct error_reset_required             : public error { error_reset_required(const std::string& msg) : error {msg} {} };
struct error_operating_system           : public error { error_operating_system(const std::string& msg) : error {msg} {} };
struct error_lib_rm_version_mismatch    : public error { error_lib_rm_version_mismatch(const std::string& msg) : error {msg} {} };
struct error_in_use                     : public error { error_in_use(const std::string& msg) : error {msg} {} };
struct error_memory                     : public error { error_memory(const std::string& msg) : error {msg} {} };
struct error_no_data                    : public error { error_no_data(const std::string& msg) : error {msg} {} };
struct error_vgpu_ecc_not_supported     : public error { error_vgpu_ecc_not_supported(const std::string& msg) : error {msg} {} };
struct error_insuffcient_resources      : public error { error_insuffcient_resources(const std::string& msg) : error {msg} {} };
struct error_freq_not_supported         : public error { error_freq_not_supported(const std::string& msg) : error {msg} {} };
struct error_unknown                    : public error { error_unknown(const std::string& msg) : error {msg} {} };
} // namespace NVMLpp::errors
