#pragma once

#include "nvmlpp/nvml_native.hpp"

namespace NVMLpp::utils
{

void throw_if_error(const char* func_name, NVML_native::nvmlReturn_t err_code);

} // namespace NVMLpp::utils

#define NVMLPP_MAKE_NATIVE_CALL(expr) \
    NVMLpp::utils::throw_if_error(__PRETTY_FUNCTION__, expr)
